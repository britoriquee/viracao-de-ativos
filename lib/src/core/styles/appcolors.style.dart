// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

class AppColors {
  static const Color SECUNDARY = Color(0xFFa058c9);
  static const Color PRIMARY = Color(0xFF461d60);

  static const Color GRAYD0 = Color(0xFFD0D0D0);
  static const Color GRAYB6 = Color(0xFFB6BCC4);
  static const Color GRAY7E = Color(0xFF7E8897);
  static const Color GRAY42 = Color(0xFF424C53);
  static const Color GRAYDA = Color(0xFFDADADA);

  static const Color SHADOW = Color.fromRGBO(166, 171, 189, 0.25);

  static const Color DISABLED = GRAYDA;
}

// ignore_for_file: constant_identifier_names

class AppRoutes {
  static const HOME = "/";
  static const PRICE_VARIATION = "/price_variation";
  static const CHART = "/chart";
}
